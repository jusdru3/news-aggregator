<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HtmlClientTest extends TestCase
{

    public function testloadContentFromUrl()
    {
        $httpClient = new \App\Client\HttpClient();
        $content = $httpClient->loadContentFromUrl("http://www.delfi.lt/rss/feeds/lithuania.xml");
        $this->assertTrue(isset($content));
    }
}
