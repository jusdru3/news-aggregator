<?php
use App\Client\HttpClientInterface;
use App\Parsers\XmlParserInterface;
use App\Repositories\Article\ArticleRepositoryInterface;
use App\Repositories\Feed\FeedRepositoryInterface;
use App\Services\RssFeederService;


class RssFeederServiceTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetAndSaveArticlesTest()
    {
        $articleRepositoryMock = new ArticleRepositoryMock();
        $feedRepositoryMock = new FeedRepositoryMock();
        $httpClientMock = new HttpClientMock();
        $xmlParserMock = new XmlParserMock();
        $rssFeederService = new RssFeederService($feedRepositoryMock,$articleRepositoryMock,$httpClientMock,$xmlParserMock);

        $rssFeederService->GetAndSaveArticles();
        $this->assertTrue(sizeof($articleRepositoryMock->all())>0);
    }
}


class ArticleRepositoryMock implements ArticleRepositoryInterface
{
    private $articles=[];
    public function all()
    {
        return $this->articles;
    }

    public function create(array $data)
    {
        array_push($this->articles,$data);
    }

    public function update($model, array $input)
    {
        // TODO: Implement update() method.
    }
}

class FeedFake{
    public function getUrl(){
        return "";
    }
    public $id =5;
}

class FeedRepositoryMock implements FeedRepositoryInterface
{

    public function all()
    {

        $obj = new FeedFake();
        return [$obj];
    }

    public function create(array $data)
    {

    }

    public function update($model, array $input)
    {
        // TODO: Implement update() method.
    }

    public function Delete($id)
    {
        // TODO: Implement Delete() method.
    }
}

class XmlParserMock implements XmlParserInterface
{

    public function parseXml($xml, $url, $feed)
    {
        $fakeXmlObject = simplexml_load_file("http://www.delfi.lt/rss/feeds/lithuania.xml",null,LIBXML_NOCDATA); //change to mock XML
        $xmlParser = new \App\Parsers\XmlParser();
        $fakeFeedObject = new stdClass();
        $fakeFeedObject->id = 5;
        $array = $xmlParser->parseXml($fakeXmlObject,"http://www.delfi.lt/rss/feeds/lithuania.xml",$fakeFeedObject);
        return [$array];
    }
}
class HttpClientMock implements HttpClientInterface
{

    public function loadContentFromUrl($url)
    {
        return new \stdClass();
    }
}