<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class XmlParserTest extends TestCase
{
    public function testParseFromXml(){
        $fakeXmlObject = simplexml_load_file("http://www.delfi.lt/rss/feeds/lithuania.xml",null,LIBXML_NOCDATA); //change to mock XML
        $xmlParser = new \App\Parsers\XmlParser();
        $fakeFeedObject = new stdClass();
        $fakeFeedObject->id = 5;
        $array = $xmlParser->parseXml($fakeXmlObject,"http://www.delfi.lt/rss/feeds/lithuania.xml",$fakeFeedObject);
        $this->assertTrue(sizeof($array)>0);
    }
}
