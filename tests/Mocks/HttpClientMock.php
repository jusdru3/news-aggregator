<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 22.39
 */

namespace test\Mocks;


use App\Client\HttpClientInterface;

class HttpClientMock implements HttpClientInterface
{

    public function loadContentFromUrl($url)
    {
        return new \stdClass();
    }
}