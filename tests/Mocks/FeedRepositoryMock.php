<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 22.38
 */

namespace test\Mocks;


use app\Repositories\Feed\FeedRepositoryInterface;

class FeedRepositoryMock implements FeedRepositoryInterface
{

    public function all()
    {

        $obj = new \stdClass();
        $obj->getUrl = function (){
            return "";
        };
        $obj->id = 5;

        return [$obj];
    }

    public function create(array $data)
    {

    }

    public function update($model, array $input)
    {
        // TODO: Implement update() method.
    }

    public function Delete($id)
    {
        // TODO: Implement Delete() method.
    }
}