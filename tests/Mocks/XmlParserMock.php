<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 22.40
 */

namespace test\Mocks;


use App\Parsers\XmlParserInterface;
use stdClass;

class XmlParserMock implements XmlParserInterface
{

    public function parseXml($xml, $url, $feed)
    {
        $fakeXmlObject = simplexml_load_file("http://www.delfi.lt/rss/feeds/lithuania.xml",null,LIBXML_NOCDATA); //change to mock XML
        $xmlParser = new \App\Parsers\XmlParser();
        $fakeFeedObject = new stdClass();
        $fakeFeedObject->id = 5;
        $array = $xmlParser->parseXml($fakeXmlObject,"http://www.delfi.lt/rss/feeds/lithuania.xml",$fakeFeedObject);
        return [$array];
    }
}