<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 22.39
 */

namespace test\Mocks;


use app\Repositories\Article\ArticleRepositoryInterface;

class ArticleRepositoryMock implements ArticleRepositoryInterface
{
private $articles;
    public function all()
    {
        return $this->articles;
    }

    public function create(array $data)
    {
        array_push($this->articles,$data);
    }

    public function update($model, array $input)
    {
        // TODO: Implement update() method.
    }
}