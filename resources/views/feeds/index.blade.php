@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <h1>Feeds List</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Url</th>
                    <th>Description</th>
                    <th>Title</th>
                    <th>Category</th>
                    {{--<th>Edit</th>--}}

                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($feeds as $feed)
                    <tr>
                        <td>{{$feed->getUrl()}}</td>
                        <td>{{$feed->getDescription()}}</td>
                        <td>{{$feed->getTitle()}}</td>
                        <td>{{$feed->category->getName()}}</td>

                        {{--<td><a class="btn" href="{site_url()}admin/editFront/2">Edit</a></td>--}}
                        <td>{!! Form::open([
            'method' => 'DELETE',
            'route' => ['feeds.delete', $feed->id]
        ]) !!}
                            {!! Form::submit('Delete ', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a href="{{route("feeds.create.post")}}" class="btn btn-info" role="button">Create feed</a>
        </div>
    </div>

@endsection