@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h1>Create Category</h1>
            {!!  Form::open(array('route' => 'categories.create.post'))  !!}
            <ul>
                <div class="form-group">
                    {!! Form::label('name', 'name:') !!}
                    {!! Form::text('name',"", array('class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    {!! Form::submit('Submit', array('class' => 'btn')) !!}
                </div>
            </ul>
            {!! Form::close() !!}

            @if ($errors->any())
                <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>'))  !!}
                </ul>
            @endif
        </div>
    </div>
@endsection