# Task 2 - News aggregator #

## Installation ##

* **clone repository**
* **composer update**
*** make file .env and copy text from .env.example, change db_connection for database and mail_driver for password restore function**
* **php artisan key:generate**
* **php artisan migrate --seed**

## Start guide ##

* user is created from terminal **php artisan user:create**
* feeds are created from web interface, you have to be logged in
* news getter service is invoked from terminal **php artisan feed:update**

# Problems #

Because of a lack of time and last 6months working with .Net I had some problems with PHP. I have not find the most PHP way for dataflow. (Arrays vs DTOs). Also I have not written unit tests because of magic PHPUnit errors. News getter are only tested with "15min" and "delfi" rss urls. It is possible that some other urls won't work properly because of differences of every rss scheme. 