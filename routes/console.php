<?php

use App\Repositories\User\ElUserRepository;
use App\Repositories\User\ElUserRepositoryInterface;
use App\Services\RssFeederServiceInterface;
use App\User;
use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('feed:update', function (RssFeederServiceInterface $rssFeederService) {
    $rssFeederService->GetAndSaveArticles();
    $this->info("completed");
});

Artisan::command('user:create', function () {
//ElUserRepositoryInterface $userRepo
    $userRepo = new ElUserRepository(new User()); // bug somewhere in IoC container . quickfix
    $name = $this->ask('What is your name?');
    $email = $this->ask('What is your email?');
    while (!filter_var($email, FILTER_VALIDATE_EMAIL )){
        $email = $this->ask('It is not an email. What is your real email?');
    }
    $password = $this->ask('What is your password?');
    $array = [];
    $array["name"]=$name;
    $array["email"]=$email;
    $array["password"]=bcrypt($password);
    $userRepo->create($array);
    $this->info("completed");
});

