<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/',"HomeController@index");

Route::group(array('prefix'=>'feeds'),function ()
{
    Route::get('/create/',"FeedsController@createGet");
    Route::post("/create/",array(
        'as'=>'feeds.create.post',
        'uses'=>"FeedsController@createPost"
    ));
    Route::get('/',"FeedsController@FeedsListGet");

    Route::delete('/{id}',array(
        'as'=>'feeds.delete',
        'uses'=>"FeedsController@FeedDelete"
    ));

});

Route::group(array('prefix'=>'categories'),function ()
{
    Route::get('/create/',"CategoriesController@createGet");
    Route::post("/create/",array(
        'as'=>'categories.create.post',
        'uses'=>"CategoriesController@createPost"
    ));

});

Auth::routes();

Route::get('/home', 'HomeController@index');
