<?php

use Illuminate\Database\Seeder;
use App\Models\Feed;

class FeedsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Feed::count() == 0) {
            $this->generate();
        }
    }
    private function generate()
    {
        DB::table("feeds")->insert([
            [
                'url'=>"http://www.15min.lt/rss",
                'category_id'=>1,
            ],
            [
                'url'=>'http://www.delfi.lt/rss/feeds/lithuania.xml',
                'category_id'=>1,
            ]
        ]);
    }
}
