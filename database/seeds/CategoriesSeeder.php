<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Category::count() == 0) {
            $this->generate();
        }
    }
    private function generate()
    {
        DB::table("categories")->insert([
            [
                'name'=>"Kriminalai",
            ],
            [
                'name'=>'Sportas',
            ]
        ]);
    }
}
