<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('feeds', function (Blueprint $table) {
            $table->foreign("category_id")
                ->references("id")
                ->on("categories")
                ->onDelete('cascade');
        });
        Schema::table('articles', function (Blueprint $table) {
            $table->foreign("feed_id")
                ->references("id")
                ->on("feeds")
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feeds', function (Blueprint $table) {
            $table->dropForeign('feeds_category_id_foreign');
        });
        Schema::table('articles', function (Blueprint $table) {
            $table->dropForeign('articles_feed_id_foreign');
        });
    }
}
