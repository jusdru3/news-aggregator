<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesRequest;
use App\Repositories\Category\CategoryRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;

class CategoriesController extends Controller
{
    protected $categoriesRepository;

    /**
     * CategoriesController constructor.
     * @param $categoriesRepository
     */
    public function __construct(CategoryRepositoryInterface $categoriesRepository)
    {
        $this->categoriesRepository = $categoriesRepository;
        $this->middleware('auth');
    }

    public function createGet(){
        return view('categories.create');
    }

    /**
     * @param CategoriesRequest $request
     * @internal param $CategoriesRequest $
     */
    public function createPost(CategoriesRequest $request){
        $this->categoriesRepository->create($request->all());
        return redirect(action("FeedsController@createGet"));
    }
}
