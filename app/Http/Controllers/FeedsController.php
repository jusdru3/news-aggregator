<?php

namespace App\Http\Controllers;

use App\Dtos\Feed\CreateFeedDto;
use App\Http\Requests\FeedsRequest;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Feed\FeedRepositoryInterface;
use App\Services\RssFeederServiceInterface;
use Illuminate\Support\Facades\Redirect;


class FeedsController extends Controller
{
    protected $categoriesRepository;
    protected $rssFeederService;
    protected $feedRepository;


    /**
     * FeedsController constructor.
     * @param CategoryRepositoryInterface $categoriesRepository
     * @param RssFeederServiceInterface $rssFeederService
     * @param FeedRepositoryInterface $feedRepository
     */
    public function __construct(CategoryRepositoryInterface $categoriesRepository,RssFeederServiceInterface $rssFeederService,
        FeedRepositoryInterface $feedRepository)
    {
        $this->categoriesRepository = $categoriesRepository;
        $this->rssFeederService = $rssFeederService;
        $this->feedRepository = $feedRepository;
        $this->middleware('auth');
    }


    public function CreateGet(){
        $categories = $this->categoriesRepository->all();
        $categoriesList = [];
        foreach ($categories as $category){
            $categoriesList[$category->id] = $category->name;
        }
//        dd($categoriesDto);
        return view('feeds.create',['categoriesList'=>$categoriesList]);
    }

    public function CreatePost(FeedsRequest $request){
        $feedDto = new CreateFeedDto($request->url,$request->category);
        $succed = $this->rssFeederService->CreateFeed($feedDto);
        return $succed? Redirect::action("FeedsController@FeedsListGet"):Redirect::back()->withErrors(['Problems with feed url.']);
    }

    public function FeedsListGet(){
        $feeds = $this->feedRepository->all();
        return view('feeds.index', ['feeds' => $feeds]);
    }

    public function FeedDelete($id){
        $this->feedRepository->delete($id);
        return back();
    }
}
