<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\Article\ArticleRepositoryInterface;
use App\Repositories\Feed\FeedRepositoryInterface;
use App\Services\RssFeederServiceInterface;
use Illuminate\Http\Request;

use App\Http\Requests;

class NewsController extends Controller
{
    protected $feedRepository;
    protected $articleRepository;
    protected $rssService;

    /**
     * NewsController constructor.
     * @param FeedRepositoryInterface $feedRepository
     * @param ArticleRepositoryInterface $articleRepository
     * @param RssFeederServiceInterface $rssService
     */
    public function __construct(FeedRepositoryInterface $feedRepository,ArticleRepositoryInterface $articleRepository,RssFeederServiceInterface $rssService)
    {
        $this->feedRepository = $feedRepository;
        $this->articleRepository = $articleRepository;
        $this->rssService = $rssService;
    }


    public function ApiGetNews(){
        $articles = $this->articleRepository->all();
        foreach($articles as $article){
            $article->category = $article->feed->category->getName();
            $article->feedProvider = $article->feed->getUrl();
        }
        return $articles;
    }

}
