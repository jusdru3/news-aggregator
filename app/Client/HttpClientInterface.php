<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 21.19
 */

namespace App\Client;


interface HttpClientInterface
{
    public function loadContentFromUrl($url);
}