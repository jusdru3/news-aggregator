<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 21.27
 */

namespace App\Client;


/**
 * Class HttpClient
 * @package App\Client
 */
class HttpClient implements HttpClientInterface
{

    /**
     * @param $url
     */
    public function loadContentFromUrl($url)
    {
        $xml = simplexml_load_file($url,null,LIBXML_NOCDATA);
        return $xml;
    }
}