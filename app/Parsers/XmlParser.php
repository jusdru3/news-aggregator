<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 21.46
 */

namespace App\Parsers;


/**
 * Class XmlParser
 * @package App\Parsers
 */
class XmlParser implements XmlParserInterface
{

    /**
     * @param $xml
     */
    public function parseXml($xml,$url,$feed)
    {
        $articles = [];
        for($i=0;$i<$xml->channel->item->count();$i++) {
            if ($xml->channel->item[$i]->children('media', true)->content && $xml->channel->item[$i]->children('media', true)->content->attributes()) {
                $image = (string)$xml->channel->item[$i]->children('media', true)->thumbnail->attributes();
            }
            else{
                $image = "";
            }
            $title = (string)$xml->channel->item[$i]->title;
            $link = (string)$xml->channel->item[$i]->link;
            if (strpos(strtolower($url), '15min.lt') !== false) { // "15min" need extra parsing
                $start = "</a>";
                $end = "<br";
                $tempDescription = (string)$xml->channel->item[$i]->description;
                $description = $this->get_string_between($tempDescription,$start,$end);
                $start = "src=\"";
                $end = "\"/>";
                $image = $this->get_string_between($tempDescription,$start,$end);
            }
            else {
                $description = (string)$xml->channel->item[$i]->description;
            }
            $pubDate = (string)$xml->channel->item[$i]->pubDate;
            $guid = (string)$xml->channel->item[$i]->guid;
            $feedId = $feed->id;
            $html = "<img src='$image' alt='$title'>";
            $html .= "<a href='$link'><h3>$title</h3></a>";
            $html .= "$description";
            $html .= "<br />$pubDate<hr />";

            //ToDo change from array to DTO
            $articleArray = array();
            $articleArray['image']=$image;
            $articleArray['title']=$title;
            $articleArray['link']=$link;
            $articleArray['description']=$description;
            $articleArray['pubDate']=$pubDate;
            $articleArray['html']=$html;
            $articleArray['guid']=$guid;
            $articleArray['feed_id']=$feedId;

            $articles[$i]=$articleArray;
        }
        return $articles;
    }

    /**
     * @param $string
     * @param $start
     * @param $end
     * @return string
     */
    private function get_string_between($string, $start, $end){
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return trim(substr($string, $ini, $len));
    }
}