<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.12.12
 * Time: 21.46
 */

namespace App\Parsers;


interface XmlParserInterface
{
    public function parseXml($xml,$url,$feed);
}