<?php

namespace App\Console\Commands;

use App\Services\RssFeederServiceInterface;
use Illuminate\Console\Command;

class FeedUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update feeds';

    protected $rssFeederService;

    /**
     * Create a new command instance.
     *
     * @param RssFeederServiceInterface $rssFeederService
     */
    public function __construct(RssFeederServiceInterface $rssFeederService)
    {

        parent::__construct();
        $this->rssFeederService = $rssFeederService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        line('Welcome to the user generator.');
    }
}
