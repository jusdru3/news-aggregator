<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.23
 * Time: 17.53
 */

namespace app\Repositories\Category;


use App\Repositories\EloquentBaseRepositoryInterface;

interface CategoryRepositoryInterface extends EloquentBaseRepositoryInterface
{

}