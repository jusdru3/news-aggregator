<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 23.50
 */

namespace app\Repositories;


interface EloquentBaseRepositoryInterface
{
    public function all();
    public function create(array $data);
    public function update($model, array $input);
}