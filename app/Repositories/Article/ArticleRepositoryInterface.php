<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 15.16
 */

namespace app\Repositories\Article;

use App\Repositories\EloquentBaseRepositoryInterface;

interface ArticleRepositoryInterface extends EloquentBaseRepositoryInterface
{

}