<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 15.16
 */

namespace App\Repositories\Article;


use App\Models\Article;
use App\Repositories\EloquentBaseRepository;

class ArticleRepository extends EloquentBaseRepository  implements ArticleRepositoryInterface
{
    protected $article;

    /**
     * ArticleRepository constructor.
     * @param $article
     */
    public function __construct($article)
    {
        parent::__construct($article);
        $this->article = $article;
    }

    public function create(array $data)
    {
        $article = Article::where("guid",$data["guid"])->first();
        if ($article==null){
            $model = $this->article->create($data);
            if (!$model) {
                return false;
            }
            return true;
        }
        return false;
    }

    public function all()
    {
        return Article::orderBy('pubDate','desc')->get();
    }

}