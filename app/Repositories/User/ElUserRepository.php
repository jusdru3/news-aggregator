<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.24
 * Time: 02.57
 */

namespace app\Repositories\User;


use App\Repositories\EloquentBaseRepository;
use App\Repositories\User\ElUserRepositoryInterface;

class ElUserRepository extends EloquentBaseRepository implements ElUserRepositoryInterface
{
    protected $user;

    /**
     * FeedRepository constructor.
     * @param null $user
     */
    public function __construct($user)
    {
        parent::__construct($user);
        $this->user = $user;
    }
}