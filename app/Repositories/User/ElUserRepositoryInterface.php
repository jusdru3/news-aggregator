<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.24
 * Time: 02.57
 */

namespace app\Repositories\User;


use App\Repositories\EloquentBaseRepositoryInterface;

interface ElUserRepositoryInterface extends EloquentBaseRepositoryInterface
{

}