<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 12.52
 */

namespace App\Repositories\Feed;


use App\Models\Feed;
use App\Repositories\EloquentBaseRepository;

class FeedRepository extends EloquentBaseRepository implements FeedRepositoryInterface
{
    protected $feed;

    /**
     * FeedRepository constructor.
     * @param $feed
     */
    public function __construct($feed)
    {

        parent::__construct($feed);
        $this->feed = $feed;
    }


    public function Delete($id)
    {
        $feed = Feed::findOrFail($id);
        $feed->delete();
    }
}