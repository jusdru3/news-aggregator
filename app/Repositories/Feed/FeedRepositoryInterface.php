<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 12.48
 */

namespace app\Repositories\Feed;


use App\Repositories\EloquentBaseRepositoryInterface;

interface FeedRepositoryInterface extends EloquentBaseRepositoryInterface
{
    public function Delete($id);
}