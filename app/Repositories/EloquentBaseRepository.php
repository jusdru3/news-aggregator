<?php

namespace App\Repositories;


/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 12.41
 */
abstract class EloquentBaseRepository
{
    protected $model;

    public function __construct($model = null)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        $model = $this->model->create($data);
        if (!$model) {
            return false;
        }
        return true;
    }

    public function update($model, array $input)
    {
        $model->fill($input);
        return $model->save();
    }
}