<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 22.40
 */

namespace App\Services;



use App\Dtos\Feed\CreateFeedDto;

interface RssFeederServiceInterface
{
    public function GetAndSaveArticles();
    public function CreateFeed(CreateFeedDto $feedDto);
}