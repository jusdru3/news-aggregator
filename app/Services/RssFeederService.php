<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.22
 * Time: 22.40
 */

namespace App\Services;


use App\Client\HttpClientInterface;
use App\Dtos\Feed\CreateFeedDto;
use App\Parsers\XmlParserInterface;
use App\Repositories\Article\ArticleRepository;
use app\Repositories\Article\ArticleRepositoryInterface;
use app\Repositories\Feed\FeedRepositoryInterface;
use DOMDocument;
use Exception;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * Class RssFeederService
 * @package app\Services
 */
class RssFeederService implements RssFeederServiceInterface
{
    /**
     * @var FeedRepositoryInterface
     */
    protected $feedRepository;
    /**
     * @var ArticleRepository
     */
    protected $articleRepository;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var XmlParserInterface
     */
    private $xmlParser;


    /**
     * RssFeederService constructor.
     * @param FeedRepositoryInterface $feedRepository
     * @param ArticleRepositoryInterface $articleRepository
     * @param HttpClientInterface $httpClient
     * @param XmlParserInterface $xmlParser
     */
    public function __construct(FeedRepositoryInterface $feedRepository, ArticleRepositoryInterface $articleRepository,
        HttpClientInterface $httpClient,XmlParserInterface $xmlParser)
    {
        $this->feedRepository = $feedRepository;
        $this->articleRepository = $articleRepository;
        $this->httpClient = $httpClient;
        $this->xmlParser = $xmlParser;
    }


    /**
     *
     */
    public function GetAndSaveArticles()
    {
        $feeds = $this->feedRepository->all();
        $v =0;
        foreach ($feeds as $feed){
            if ($v!=20)
            $this->GetArticlesFromFeedUrl($feed);
            $v++;
        }

    }
    //Todo refactoring
    /**
     * @param $feed
     */
    private function GetArticlesFromFeedUrl($feed){
        $url = $feed->getUrl();
        $xml = $this->httpClient->loadContentFromUrl($url);
        $articles = $this->xmlParser->parseXml($xml,$url,$feed);

        foreach ($articles as $article) {
            $this->articleRepository->create($article);
        }
    }


    /**
     * @param CreateFeedDto $feedDto
     * @return bool
     */
    public function CreateFeed(CreateFeedDto $feedDto)
    {
        try {
//            $xml = simplexml_load_file($feedDto->getUrl(), null, LIBXML_NOCDATA);
            $xml = $this->httpClient->loadContentFromUrl($feedDto->getUrl());
            $description = $xml->channel->description;
            $title = $xml->channel->title;

            $feedArray = [];
            $feedArray['description']=$description;
            $feedArray['title']=$title;
            $feedArray['url']=$feedDto->getUrl();
            $feedArray['category_id']=$feedDto->getCategoryId();

            $this->feedRepository->create($feedArray);
            return true;
        }
        catch(Exception $e){
            return false;
        }
    }


}