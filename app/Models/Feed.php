<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    public function setUrl($value)
    {
        $this->url = $value;
    }
    public function getUrl()
    {
        return $this->url;
    }
    public function setDescription($value)
    {
        $this->description = $value;
    }
    public function getDescription()
    {
        return $this->description;
    }
    public function setTitle($value)
    {
        $this->title = $value;
    }
    public function getTitle()
    {
        return $this->title;
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    protected $fillable = [
        'url', 'description', 'title', 'category_id'
    ];



}
