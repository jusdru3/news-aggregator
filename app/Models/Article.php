<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public function setName($value)
    {
        $this->name = $value;
    }
    public function getName()
    {
        return $this->name;
    }

    protected $fillable = [
        'image','description', 'title', 'link','pubDate','html','guid','feed_id'
    ];

    public function feed(){
        return $this->belongsTo(Feed::class);
    }

}
