<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function setName($value)
    {
        $this->name = $value;
    }
    public function getName()
    {
        return $this->name;
    }
    protected $fillable = [
        'name'
    ];
}
