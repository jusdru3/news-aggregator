<?php

namespace App\Providers;

use App\Client\HttpClient;
use App\Parsers\XmlParser;
use App\Services\RssFeederService;
use Illuminate\Support\ServiceProvider;

class MyServicesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;
        $app->bind('app\Services\RssFeederServiceInterface', function ($app) {

            return new RssFeederService($app->make('app\Repositories\Feed\FeedRepositoryInterface'),
                $app->make('app\Repositories\Article\ArticleRepositoryInterface'),
                $app->make('app\Client\HttpClientInterface'),
                $app->make('app\Parsers\XmlParserInterface')
            );
        });

        $app->bind('app\Client\HttpClientInterface',function ( $app){
            return new HttpClient();
        });

        $app->bind('app\Parsers\XmlParserInterface',function ( $app){
            return new XmlParser();
        });
    }
}
