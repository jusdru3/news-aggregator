<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Feed;
use App\Repositories\Article\ArticleRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Feed\FeedRepository;
use app\Repositories\User\ElUserRepository;
use App\Repositories\User\UserRepository;
use App\User;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;
        $app->bind('app\Repositories\Feed\FeedRepositoryInterface', function ($app) {
            return new FeedRepository(new Feed());
        });
        $app->bind('app\Repositories\Article\ArticleRepositoryInterface', function ($app) {
            return new ArticleRepository(new Article());
        });
        $app->bind('app\Repositories\Category\CategoryRepositoryInterface', function ($app) {
            return new CategoryRepository(new Category());
        });
        $app->bind('app\Repositories\Category\ElUserRepositoryInterface', function ($app) {
            return new ElUserRepository(new User());
        });
    }
}
