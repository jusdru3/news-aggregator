<?php
/**
 * Created by PhpStorm.
 * User: justinas
 * Date: 16.10.23
 * Time: 18.37
 */

namespace App\Dtos\Feed;


class CreateFeedDto
{
    private $url;
    private $categoryId;

    /**
     * CreateFeedDto constructor.
     * @param $url
     * @param $categoryId
     */
    public function __construct($url, $categoryId)
    {
        $this->url = $url;
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }


}